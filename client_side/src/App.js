import React, { Component } from 'react';
import SearchBar from './components/SearchBar';
import CardTask from './components/CardTask';
import Paginate from './components/Paginate';
import SideBar from './components/SideBar';
import {Row, Col } from 'reactstrap';
import './App.css'

class App extends Component {


  constructor(props) {
    super(props);

    this.state = {
      allData:[],
      data: [],
      startCard : 0,
      endCard : 8,
      startPage : 1,
      endPage : 6
    };

  }

  componentDidMount() {
    fetch((process.env.REACT_APP_API ||  'http://localhost:9000') + '/list')
      .then(response => response.json())
      .then(data => {
        this.setState({
          allData : data.cards,
          data : data.cards
        });

      });
  }

  handleNavigation = (pageNo) => {
    let startCard = (parseInt(pageNo) - 1 ) * 9;
    let endCard = startCard + 8 > this.state.data.length ? this.state.data.length : startCard + 8;
    this.setState({
      startCard,
      endCard
    })
  }

  handleNavigationNext = () => {
    let endPage = this.state.endPage > Math.ceil(this.state.data.length/8) - 5 ? Math.ceil(this.state.data.length/8) + 1 : this.state.endPage + 5;
    let startPage = endPage - 5 < 0 ? 1 : endPage - 5;
    console.log(startPage,endPage);
    this.setState({
      startPage,
      endPage,
    })
  }

  handleNavigationPrev = () => {
    let startPage = this.state.startPage !== 1 ? this.state.startPage - 5 : 1;
    let endPage = this.state.endPage + 5 > Math.ceil(this.state.data.length/8) ? Math.ceil(this.state.data.length/8) + 1 : startPage + 5;
    this.setState({
      startPage,
      endPage,
    })
  }

  handleSearch = (name) => {
    name = name.toLowerCase();
    let data = this.state.allData.filter(card => {
      return card.name.toLowerCase().includes(name);
    });
    let endPage = Math.ceil(data.length/8) < 5 ?  Math.ceil(data.length/8) + 1 : 6;
    this.setState({
      data,
      startCard : 0,
      endCard : 8,
      startPage : 1,
      endPage
    })
  }

  render() {

    return (
      <Row className="d-flex align-items-stretch">
        <Col className="mt-5" sm="0" md={{ size: 4 }} lg="3">
          <SideBar className="sideBar" dataLength={this.state.data.length} allDataLength={this.state.allData.length}/>
        </Col>
        <Col sm="12" md={{ size: 8 }} lg="9">
          <Row className="mt-5 mb-5 mr-2 ml-2">
            <Col sm="12" md={{ size: 6, offset: 3 }} lg={{ size: 8, offset: 2 }}>
              <SearchBar onSearch={this.handleSearch}/>
            </Col>
          </Row>
          <Row className="mr-3 ml-3">

            {this.state.data.slice(this.state.startCard,this.state.endCard).map(function(card, i){
              return (
                <Col xs="6" sm="6" md="4" lg="3" className="mt-3"  key={i}>
                  <CardTask features={JSON.stringify(card)}/>
                </Col>
              );
            })}

          </Row>

          <Row className='d-flex justify-content-center'>
            <Col className='mt-3 d-flex justify-content-center' sm={{ size: 6 }} md={{ size: 6 }}>
                <Paginate start={this.state.startPage} end={this.state.endPage} onNavigation={this.handleNavigation} onNavigationNext={this.handleNavigationNext} onNavigationPrev={this.handleNavigationPrev} />
            </Col>
          </Row>
        </Col>
      </Row>

    );
  }
}

export default App;
