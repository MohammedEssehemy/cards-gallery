import React from 'react';
import logo from '../user.png';
import './SideBar.css';
import {Row, Col } from 'reactstrap';

export default class SideBar extends React.Component {



  render(){

    return (
      <div className="sticky-top">
        <div className="d-flex justify-content-center">
          <img src={logo} alt="user" className="userImg"/>
        </div>
        <div className="d-flex justify-content-center">
          <h4 className="d-flex justify-content-center">John Doe</h4>
        </div>
        <div className="d-flex justify-content-center">
          <h6 className="d-flex justify-content-center">Fixed Solutions</h6>
        </div>

        <div className="d-flex justify-content-center">
          <Row>
            <Col sm="4" md={{ size: 4 }}>
              <div className="d-flex justify-content-center mt-5">
              <h2 className="d-flex justify-content-center">{this.props.dataLength}</h2>
              </div>
              <div className="d-flex justify-content-center">
                Displayed
              </div>
            </Col>

            <Col>
              <div className="d-flex justify-content-center  mt-5">
                <h2 className="d-flex justify-content-center">/</h2>
              </div>
            </Col>

            <Col className="mb-3" sm="4" md={{ size: 4 }}>
              <div className="d-flex justify-content-center  mt-5">
                <h2 className="d-flex justify-content-center">{this.props.allDataLength}</h2>
              </div>
              <div className="d-flex justify-content-center">
                All
              </div>
            </Col>
          </Row>
        </div>
      </div>
      
      
    );

  }

}