// express config
const express = require('express');
const server = express();

// DB Config
const mongoose = require('mongoose');
const dbURI = process.env.MONGO_URI || 'mongodb://localhost:27017/fixed_task';
mongoose.connect(dbURI, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open',  () => {
  console.log("Connected to database successfully");
});
const cardModel = require('./models/cards');

server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// list api
server.get('/list',(req, res) => {

  cardModel.find({},(err,cards)=>{
    if(err){
      res.statusCode(400).json({
        message : 'error with the database'
      });
    } else {
      res.status(200).json({
        cards
      });
    }

  });

});

var port = 9000;
server.listen(port,()=>{
    console.log("server  on");
});
