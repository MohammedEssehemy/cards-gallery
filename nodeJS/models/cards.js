const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cardSchema = new Schema({
  amiiboSeries : String,
  character: String,
  gameSeries: String,
  head: String,
  image: String,
  name: String,
  release: {
    au: String,
    eu: String,
    jp: String,
    na: String
  },
  tail: String,
  type: String
});

mongoose.model('cards',cardSchema);

module.exports = mongoose.model('cards');
